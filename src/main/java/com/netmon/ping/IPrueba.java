package com.netmon.ping;

public interface IPrueba {
		
	public boolean ejecutarPrueba(String ip, Integer frecuencia);
	
	public void mostrarInforme();

}