package com.netmon.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.netmon.ping.Ping;

public class TestPrueba {

	Ping ping = new Ping();

	@Test
	public void MonitorNodos_hacerPing() {
		assertTrue(ping.ejecutarPrueba("127.0.0.1", 1000) == true);

		assertTrue(ping.ejecutarPrueba("1272.0.0.1", 1000) == false);

		assertTrue(ping.ejecutarPrueba(".0.0.", 1000) == false);

		assertTrue(ping.ejecutarPrueba(",ip,", 1000) == false);

		assertTrue(ping.ejecutarPrueba("ip", 1000) == false);

		assertTrue(ping.ejecutarPrueba("11", 1000) == false);

		assertTrue(ping.ejecutarPrueba(" ", 1000) == false);
	}

}